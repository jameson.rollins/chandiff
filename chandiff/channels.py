import logging
import fnmatch
import nds2
from . import nds


SP_SUFFIX = [
    'GAIN',
    'OFFSET',
    'TRAMP',
    'LIMIT',
    'SWSTAT',
    'SWMASK',
    'SWREQ',
    ]


def is_sp(chan):
    """Check if channel name corresponds to a setpoint

    Currently use heuristic based on suffix

    """
    s = chan.split('_')[-1]
    if s in SP_SUFFIX:
        return True
    return False


def load_channels_fileobj(f, globs):
    """Load channel list globs from file

    """
    cl = []
    for line in f:
        chan = line.split()[0]
        # if not fnmatch.fnmatch(chan, glob):
	#     continue
        if not is_sp(chan):
            continue
	cl.append(chan)
    chans = set()
    for glob in globs:
        chans |= set(fnmatch.filter(cl, glob))
    return list(chans)


def fetch_channels_time(time,
                        globs,
                        ctype=nds2.channel.CHANNEL_TYPE_RAW,
                        dtype=nds2.channel.DEFAULT_DATA_MASK,
                        minrate=nds2.channel.MIN_SAMPLE_RATE,
                        maxrate=16,
                        ):
    """Fetch channel list globs for time from NDS

    """
    time = int(time)
    conn = nds.connection()
    #conn.set_epoch('{}-{}'.format(time, time+1))
    conn.set_epoch(time, time+1)
    chans = set()
    for glob in globs:
        cl = conn.find_channels(glob, ctype, dtype, minrate, maxrate)
        chans |= set([c.name for c in cl if is_sp(c.name)])
    return list(chans)
