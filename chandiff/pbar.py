import os
import logging

try:
    from tqdm import tqdm
except ImportError:
    pass
    
class LoggingPbar(object):
    def __init__(self, time, total):
        self.time = time
        self.total = total
        self.steps = 0
    def __enter__(self, *args):
        return self
    def update(self, *args):
        self.steps += 1
        if self.steps % 100 == 0:
            logging.debug('fetched {}: {}/{}'.format(self.time, self.steps, self.total))
        if self.steps == self.total:
            logging.debug('fetched {}: {}/{} DONE'.format(self.time, self.steps, self.total))
    def __exit__(self, *args):
        return

def get_search_pbar():
    pbar = None
    if not logging.getLogger().isEnabledFor(logging.DEBUG):
        try:
            pbar = tqdm(desc="searching")
        except NameError:
            logging.warning("NOTE: install tqdm module for progress bar")
    return pbar

def get_fetch_pbar(total, time, pos, display=True):
    if display and not logging.getLogger().isEnabledFor(logging.DEBUG):
        try:
            # see https://github.com/tqdm/tqdm#parameters
            # for PBAR_FMT
            pbar = tqdm(total=total,
                        desc="fetching {}".format(time),
                        unit="chan",
                        position=pos,
                        bar_format=os.getenv('PBAR_FMT'),
                        )
        except NameError:
            logging.warning("NOTE: install tqdm module for progress bar")
    else:
        pbar = LoggingPbar(time, total)
    return pbar
