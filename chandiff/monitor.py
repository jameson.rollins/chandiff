import fnmatch
from tqdm import tqdm
import logging
from epics import ca

import pcaspy
import threading


def match_globs(globs, name):
    if not globs:
        return True
    for glob in globs:
        if fnmatch.fnmatch(name, glob):
            return True
    return False


class LoggingPbar(object):
    def __init__(self, desc, total=None, time=None):
        self.desc = desc
        self.time = time
        self.total = total
        self.steps = 0
        logging.debug('{}...'.format(desc))
    def __enter__(self, *args):
        return self
    def update(self, *args):
        if not self.total:
            return
        self.steps += 1
        if self.steps % 100 == 0:
            logging.debug('{} {}: {}/{}'.format(self.desc, self.time, self.steps, self.total))
        if self.steps == self.total:
            logging.debug('{} {}: {}/{} DONE'.format(self.desc, self.time, self.steps, self.total))
    def close(self):
        return
    def __exit__(self, *args):
        return self.close()


#class ChanMonitor(object):
class ChanMonitor(threading.Thread):
    def __init__(self, channels):
        self.channels = channels

    def __get_pbar(self, desc):
        # logging.debug("creating pvs...")
        pbar = tqdm(desc=desc, total=len(self.channels))
        # pbar = LoggingPbar(desc)
        return pbar

    def __subscription_callback(self, *args, **kwargs):
        print((args, kwargs))
        self.pvs[kwargs['pvname']] = kwargs['value']

    # http://cars9.uchicago.edu/software/python/pyepics3/advanced.html#strategies-for-connecting-to-a-large-number-of-pvs
    def connect(self):
        self.pvs = {}

        pbar = self.__get_pbar("create pvs")
        for name in self.channels:
            chid = ca.create_channel(name, connect=False, auto_cb=False)
            self.pvs[name] = (chid, None)
            pbar.update()
        pbar.close()

        pbar = self.__get_pbar("connect")
        for name, pv in self.pvs.iteritems():
            ca.connect_channel(pv[0])
            pbar.update()
        pbar.close()

        logging.debug("poll")
        ca.poll()

        pbar = self.__get_pbar("subscribe")
        for name, pv in self.pvs.iteritems():
            cb = ca.create_subscription(
                name,
                callback=self.__subscription_callback)
            # self.pvs[name] = (pv[0], val)
            pbar.update()
        pbar.close()

        pbar = self.__get_pbar("get data")
        for name, pv in self.pvs.iteritems():
            ca.get(pv[0], wait=False)
            pbar.update()
        pbar.close()

        logging.debug("poll")
        ca.poll()

        pbar = self.__get_pbar("complete")
        for name, pv in self.pvs.iteritems():
            val = ca.get_complete(pv[0])
            self.pvs[name] = (pv[0], val)
            pbar.update()
        pbar.close()


    def get(self, globs=None):
        """generator for values of connected channels

        """
        for name, data in self.pvs.iteritems():
            if match_globs(globs, name):
                yield (name, data[1])

    def run(self):
        self.connect()


db = {
    'DIFFS': {
        'type': 'int',
        'value': -1,
        'lolo': -1,
        'low': -1,
    },
}

class CADriver(pcaspy.Driver):
    def read(self, channel):
        value = self.getParam(channel)
        # print('%s == %s' % (channel, value))
        return value

    def write(self, channel, value):
        # print('%s => %s' % (channel, value))
        self.setParam(channel, value)
        return True

class CAServer(threading.Thread):
    def __init__(self, system_name):
        super(CAServer, self).__init__()
        ifo = 'T1'
        self.prefix = '%s:GRD-%s_' % (ifo, system_name)
        self._server = pcaspy.SimpleServer()
        #self._server.setDebugLevel(4)
        self._loaddb()
        self._driver = CADriver(self._request_event)
        # clear initial alarms causing UDF.INVALID status
        # FIXME: this should not be necessary, as the alarm states
        # should already be initalized properly.  pcaspy bug?
        for chan in guarddb:
            self._driver.setParamStatus(chan, pcaspy.Severity.NO_ALARM, pcaspy.Severity.NO_ALARM)
        self._running = True
        self.daemon = True

##################################################

def main():
    channels = find_channels(None, args)
    mon = monitor.ChanMonitor(channels)
    mon.connect()
    mon.get()
    # app.run(debug=True)

if __name__ == '__main__':
    main()
