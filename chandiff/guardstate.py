import os
import sys
import logging
import numpy as np

from . import nds
from . import fetch
from .pbar import get_search_pbar

def state_chan(ifo, node):
    if ifo:
        return '{}:GRD-{}_STATE_N'.format(ifo, node)
    else:
        return 'GRD-{}_STATE_N'.format(node)

def fetch_state_time(ifo, node, time):
    """return state index for system at specified time"""
    chan = state_chan(ifo, node)
    cf = fetch.ChanFetch([chan])
    data = list(cf(time))
    return int(list(data)[0][1])

# def find_last(node, state):
#     time0 = gpstime.gpsnow() - 1000
#     time1 = time0 - 24*3600
#     first = True
#     for t in find_transitions(node, time0, time1):
#         if first:
#             if t[0] == state:
#                 continue
#             first = False
#         logging.debug(t)
#         if t[1] == state:
#             return t[0]

def find_last(ifo, node, state, ref_time, pbar=False):
    """find last instance of state before reference time"""
    channel = state_chan(ifo, node)
    conn = nds.connection()
    logging.debug(conn)
    t0 = int(ref_time)
    # FIXME: optimize this number
    SEG_LENGTH = 3600
    i = 0
    if pbar:
        pbar = get_search_pbar()
    while True:
        i += 1
        t1 = t0
        t0 = t1 - i*SEG_LENGTH
        try:
            pbar.update()
        except:
            logging.debug("searching for state {}...".format((t0,t1)))
        for buf in conn.iterate(t0, t1, [channel]):
            buf = buf[0]
            state_array = buf.data.astype(int)
            ind = np.where(state_array == state)[0]
            if ind.any():
                # take last instance of matching state
                index = ind[-1]
                time = buf.gps_seconds + index*(1./buf.channel.sample_rate)
                return time
    if pbar:
        pbar.close()

# def find_last_state(node):
#     time0 = gpstime.gpsnow() - 1000
#     time1 = time0 - 24*3600
#     current = None
#     for t in find_transitions(node, time0, time1):
#         logging.debug(t)
#         if not current:
#             current = t[1]
#             logging.debug("current state: {}".format(current))
#             continue
#         if t[1] == current:
#             return (current, time0, t[0])
