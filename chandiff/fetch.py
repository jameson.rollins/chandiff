import os
import sys
import logging
import scipy.stats
from multiprocessing import Queue, Process
# from Queue import Queue
# from threading import Thread as Process
from itertools import chain

from . import nds
from .pbar import get_fetch_pbar

# Chris W's version from MEDM time machine
#
# def fetchBufs(chanList, start):
#     """Recursively fetch all available channels in chanList.
#
#     """
#     global chansFetched
#     maxChans = 100
#     if len(chanList) < 1:
#         return []
#     if len(chanList) > maxChans:
#         bufs = []
#         for n in range(0, len(chanList), maxChans):
#             bufsN = fetchBufs(chanList[n:n+maxChans], start)
#             bufs.extend(bufsN)
#         return bufs
#     try:
#         bufs = conn.fetch(start, start+1, chanList)
#         chansFetched += len(chanList)
#         return list(bufs)
#     except RuntimeError:
#         if len(chanList) == 1:
#             chansFetched += len(chanList)
#             print 'Failed to fetch channel', chanList[0]
#             return []
#         chanList1 = chanList[:len(chanList)/2]
#         chanList2 = chanList[len(chanList)/2:]
#         return fetchBufs(chanList1, start) + fetchBufs(chanList2, start)

class ChanFetch(object):
    FETCH_LIMIT = 100
    DELTA_T = 1

    def __init__(self, channels):
        """
        `channel`: Channel list to fetch

        """
        self.channels = channels
        self.nchannels = len(channels)

    def fetch_bufs_iter(self, conn, channels, time):
        """Recursively fetch all available channels in chanList.

        """
        t0 = int(time)
        t1 = int(time + self.DELTA_T)
        if len(channels) < 1:
            return
        if len(channels) <= self.FETCH_LIMIT:
            try:
                bufs = conn.fetch(t0, t1, channels)
                for buf in bufs:
                    yield (buf.channel.name, buf)
            except RuntimeError:
                # if we hit a channel error, divide and conquer until we
                # narrow down to the bad channel
                # FIXME: is there a more efficient way to do this?
                if len(channels) == 1:
                    #logging.warning('Failed to fetch channel: {}'.format(channels[0]))
                    yield (channels[0], None)
                    return
                sl1 = channels[:len(channels)/2]
                sl2 = channels[len(channels)/2:]
                for data in chain(self.fetch_bufs_iter(conn, sl1, time),
                                  self.fetch_bufs_iter(conn, sl2, time)):
                    yield data
        else:
            # break up full channel list into sublists each with FETCH_LIMIT
            # elements
            sublists = [channels[i:i+self.FETCH_LIMIT]
                        for i in range(0, len(channels), self.FETCH_LIMIT)]
            for sublist in sublists:
                for data in self.fetch_bufs_iter(conn, sublist, time):
                    yield data

    def __call__(self, time):
        """Fetch channel data for time.

        Return (channel, value) tuple.

        """
        db = {}
        conn = nds.connection()
        for (chan, buf) in self.fetch_bufs_iter(conn, self.channels, time):
            if buf:
                # FIXME: what's the best value to return here?  maybe
                # value at closest available time sample?
                val = scipy.stats.mode(buf.data)[0][0]
            else:
                val = None
            yield (chan, val)


class ChanFetchDB(object):
    """Manager object of channel values as times

    Object is initialized with a channel list, and operates as a
    dictionary keyed by times, each time returning a (channel, value)
    dictionary.

    """
    def __init__(self, channels, pbar=False):
        self.fetch = ChanFetch(channels)
        self.nchans = len(channels)
        self.db = {}
        self.pbar = pbar

    def __fetch(self, time):
        db = {}
        for c,v in self.fetch(time):
            db[c] = v
        return db

    def populate_multi(self, *times):
        """Populate the database for a specified list of times

        """
        times = [time for time in times if time not in self.db]
        if not times:
            return
        logging.debug("multi-fetch: {}".format(times))
        # FIXME: use Process.map()
        queue_in = Queue()
        queue_out = Queue()
        procs = []
        def worker(qin, qout, n):
            time = qin.get()
            db = {}
            with get_fetch_pbar(self.nchans, time, n, display=self.pbar) as pbar:
                for c,v in self.fetch(time):
                    db[c] = v
                    pbar.update()
            qout.put((time, db))
        for n,time in enumerate(times):
            queue_in.put(time)
            p = Process(target=worker, args=(queue_in, queue_out, n))
            procs.append(p)
            p.start()
        queue_in.close()
        # iterate over times just as a proxy for the number of times.
        # index by time returned from queue
        for time in times:
            out = queue_out.get()
            self.db[out[0]] = out[1]
        queue_out.close()
        for p in procs:
            p.join()
            logging.debug(p)

    def __getitem__(self, time):
        """Return (channel, value) dictionary for time

        """
        self.populate_multi(time)
        return self.db[time]

    def diff_iter(self, time0, time1):
        """Generator of channel differences between two times

        """
        self.populate_multi(time0, time1)
        for chan, val1 in self[time1].iteritems():
            val0 = self[time0][chan]
            if val0 != val1:
                yield (chan, val0, val1)
