from __future__ import print_function
import os
import io
import sys
import signal
import IPython
import logging
import argparse
import textwrap
import collections

from gpstime import gpstime

from . import channels
from . import fetch
from . import monitor

##################################################

DEFAULT_CHANNEL_GLOB = '*'

logging.basicConfig(format='%(message)s',
                    level=os.getenv('LOG_LEVEL', 'INFO').upper(),
                    )

##################################################
# The following is a custom subcommand CLI implementation based on
# callable objects.  Each command is represented by Cmd object, whose
#
#  * docstring is the command documentation
#  * self.parser attribute is the ArgumentParser
#  * __call__ method is the command execution
#
# It's an attempt to make things as self documenting as possible.
# Cmds are registered in the CMDS and ALIAS dictionaries.  The
# functions at the bottom parse the command line and generate help and
# man output.  To create a new subcommand simply define a new Cmd and
# register it in CMDS.
#
# FIXME: support options to the base command.

PROG = 'stiff'
DESC = ''

SYNOPSIS = '{} <command> [<args>...]'.format(PROG)

# NOTE: double spaces are interpreted by text2man to be paragraph
# breaks.  NO DOUBLE SPACES.  Also two spaces at the end of a line
# indicate an element in a tag list.
MANPAGE = """
NAME
  {prog} - {desc}

SYNOPSIS
  {synopsis}

DESCRIPTION

  Foo.

COMMANDS

{{cmds}}

AUTHOR
    Jameson Graef Rollins <jameson.rollins@ligo.org>
""".format(prog=PROG,
           desc=DESC,
           synopsis=SYNOPSIS,
           ).strip()

class Cmd(object):
    """base class for commands"""
    def __init__(self):
        """Initialize argument parser"""
        self.parser = argparse.ArgumentParser(
            prog='{} {}'.format(PROG, self.cmd),
            description=self.__doc__.strip(),
            # formatter_class=argparse.RawDescriptionHelpFormatter,
        )

    @property
    def cmd(self):
        return type(self).__name__.lower()

    def parse_args(self, args):
        """Parse arguments and returned ArgumentParser Namespace object"""
        return self.parser.parse_args(args)

    def __call__(self, args):
        """Take Namespace object as input and execute command"""
        pass

##########

class TimeAction(argparse.Action):
    def __init__(self, *args, **kwargs):
        argparse.Action.__init__(self, *args, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        gt = gpstime.parse(values)
        logging.debug('{}: {}'.format(self.dest, gt))
        setattr(namespace, self.dest, int(gt.gps()))

# FIXME: would be nice if this could be a argparse.Action, but
# channels depend on mutliple arguments
def add_channel_args(parser):
    parser.add_argument('-f', '--channel-file', metavar='FILE',
                        type=argparse.FileType('r'),
                        help="load channel list from file ('-' from stdin)")
    parser.add_argument('channel_glob', nargs='*', default=DEFAULT_CHANNEL_GLOB,
                        help="channel filter globs [{}]".format(DEFAULT_CHANNEL_GLOB))

def add_pbar_opt(parser):
    parser.add_argument('-np', '--no-pbar', dest='pbar', action='store_false',
                        help="disable progress bars")

def find_channels(time, args):
    """find channels from args"""
    globs = args.channel_glob
    fn = args.channel_file
    logging.debug("channel glob: {}".format(' '.join(globs)))
    if fn:
        logging.info("loading channels from '{}'...".format(fn.name))
        chans = channels.load_channels_fileobj(fn, globs)
    elif time:
        logging.info("finding channels @ {}...".format(time))
        chans = channels.fetch_channels_time(time, globs)
    else:
        sys.exit("No channels specified.")
    if len(chans) == 0:
        sys.exit("No channels found.")
    else:
        logging.info("{} channels".format(len(chans)))
    return chans

##########

class At(Cmd):
    """Show values at time.

    """
    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument('time', action=TimeAction,
                                 help="query time")
        add_channel_args(self.parser)
        add_pbar_opt(self.parser)

    def __call__(self, args):
        time = args.time
        channels = find_channels(time, args)
        db = fetch.ChanFetchDB(channels, pbar=args.pbar)
        logging.info("fetching values @ {}...".format(time))
        bad = []
        for chan, val in db[time].iteritems():
            if val:
                print('{} {}'.format(chan, val))
            else:
                bad += [chan]
        if bad:
            logging.warning("Warning: {}/{} channels failed.".format(len(bad), len(chans)))

class Diff(Cmd):
    """Show differences between two times.

    Print differences between settings values between two times.
    Settings without differences will not be displayed.

    """
    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument('time0', action=TimeAction,
                                 help="first query time")
        self.parser.add_argument('time1', action=TimeAction,
                                 help="second query time")
        add_channel_args(self.parser)
        add_pbar_opt(self.parser)

    def __call__(self, args):
        time0 = args.time0
        time1 = args.time1
        channels = find_channels(time0, args)
        db = fetch.ChanFetchDB(channels, pbar=args.pbar)
        logging.info("fetching diffs {}->{}...".format(time0, time1))
        diffs = list(db.diff_iter(time0, time1))
        if args.pbar:
            print('\n\n', file=sys.stderr)

        state_s0 = ''
        state_s1 = ''

        if not diffs:
            logging.info("No differences found.")
            return
        wc = max([len(d[0]) for d in diffs])
        wn = max(20, len(state_s0), len(state_s1))
        #tformat = '{:<{wc}} {:>{wn}} {:>{wn}}'
        tformat = '{{:{}}} {{:>{}}} {{:>{}}}'.format(wc, wn, wn)
        logging.info(tformat.format('channel', time0, time1))
        if state_s0 and state_s1:
            logging.info(tformat.format('', state_s0, state_s1))
        logging.info(tformat.format(wc*'-', wn*'-', wn*'-'))
        for diff in diffs:
            print(tformat.format(*diff))
        logging.info(tformat.format(wc*'-', wn*'-', wn*'-'))
        logging.info("{} differences found.".format(len(diffs)))

class List(Cmd):
    """List changes between times.

    """

class Monitor(Cmd):
    """Monitor settings in real-time.

    """
    def __init__(self):
        Cmd.__init__(self)
        # rgroup = self.parser.add_mutually_exclusive_group(required=True)
        # rgroup.add_argument('-s', '--snapfile', 
        #                     type=argparse.FileType('r'),
        #                     help="reference snapshot file ('-' from stdin)")
        # rgroup.add_argument('-t', '--reftime', 
        #                     help="reference snapshot file ('-' from stdin)")
        add_channel_args(self.parser)
        add_pbar_opt(self.parser)

    def __call__(self, args):
        channels = find_channels(None, args)
        mon = monitor.ChanMonitor(channels)
        mon.connect()
        # import pdb; pdb.set_trace()
        # IPython.embed(banner1=None)
        for v in mon.get('*-9*'):
            print(v)

class Channels(Cmd):
    """Print channel list for specified time to stdout.

    """
    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument('time', action=TimeAction,
                                 help="query time")
        add_channel_args(self.parser)
        self.parser.add_argument('-c', '--count', action='store_true',
                                 help="show count instead of list")

    def __call__(self, args):
        channels = find_channels(args.time, args)
        if args.count:
            print(len(channels))
        else:
            for channel in channels:
                print(channel)

class Help(Cmd):
    """Print manpage or command help (also '-h' after command).

    """
    def __init__(self):
        Cmd.__init__(self)
        self.parser.add_argument('cmd', nargs='?',
                                 help="command")

    def __call__(self, args):
        if args.cmd:
            get_func(args.cmd).parser.print_help()
        else:
            print(MANPAGE.format(cmds=format_commands(man=True)))

CMDS = collections.OrderedDict([
    ('at', At),
    ('diff', Diff),
    # ('list', List),
    ('monitor', Monitor),
    ('channels', Channels),
    ('help', Help),
    ])

ALIAS = {
    'mon': 'monitor',
    'chans': 'channels',
    '--help': 'help',
    '-h': 'help',
    }

##################################################

def format_commands(man=False):
    prefix = ' '*8
    wrapper = textwrap.TextWrapper(
        width=70,
        initial_indent=prefix,
        subsequent_indent=prefix,
        )
    with io.StringIO() as f:
        for name, func in CMDS.items():
            if man:
                fo = func()
                usage = fo.parser.format_usage()[len('usage: {} '.format(PROG)):].strip()
                desc = wrapper.fill('\n'.join([l.strip() for l in fo.parser.description.splitlines() if l]))
                f.write(u'  {}  \n'.format(usage))
                f.write(unicode(desc+'\n'))
                f.write(u'\n')
            else:
                desc = func.__doc__.splitlines()[0]
                f.write(u'  {:10}{}\n'.format(name, desc))
        output = f.getvalue()
    return output.rstrip()

def get_func(cmd):
    if cmd in ALIAS:
        cmd = ALIAS[cmd]
    try:
        return CMDS[cmd]()
    except KeyError:
        print('Unknown command:', cmd, file=sys.stderr)
        print("See 'help' for usage.", file=sys.stderr)
        sys.exit(1)

def main():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    if len(sys.argv) < 2:
        print('Command not specified.', file=sys.stderr)
        print('usage: '+SYNOPSIS, file=sys.stderr)
        print(file=sys.stderr)
        print(format_commands(), file=sys.stderr)
        sys.exit(1)
    cmd = sys.argv[1]
    func = get_func(cmd)
    args = func.parse_args(sys.argv[2:])
    logging.debug(args)
    func(args)

##################################################

if __name__ == '__main__':
    main()
