import os
import nds2

def connection():
    port = 31200
    NDSSERVER = os.getenv('NDSSERVER')
    if NDSSERVER:
        hostport = NDSSERVER.split(',')[0].split(':')
        host = hostport[0]
        if len(hostport) > 1:
            port = int(hostport[1])
    else:
        raise ValueError('NDSSERVER not specified')
    return nds2.connection(host, port)
